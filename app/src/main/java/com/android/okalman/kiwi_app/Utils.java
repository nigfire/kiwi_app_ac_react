package com.android.okalman.kiwi_app;

import android.content.Context;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by okalman on 2.10.17.
 * General handy methods are here
 */

public class Utils {

    public static long getCurrentDayAsLong() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTimeInMillis();

    }

    public static boolean fileExist(String fname, Context context) {
        File file = context.getFileStreamPath(fname);
        return file.exists();
    }

    public static HashMap<String,String>  getParamsForApi(){
        HashMap<String ,String> params = new HashMap<>();
        String currentDate = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
        Date future = Calendar.getInstance().getTime();
        Calendar c = Calendar.getInstance();
        c.setTime(future);
        c.add(Calendar.DATE, 3);
        future = c.getTime();
        String futureDate = new SimpleDateFormat("dd/MM/yyyy").format(future);
        params.put("flyFrom", "CZ");
        params.put("dateFrom", currentDate);
        params.put("dateTo", futureDate);
        params.put("partner","picky");
        params.put("partner_market","us");
        params.put("maxstopovers","2");
        params.put("limit","30");
        return params;

    }
}
