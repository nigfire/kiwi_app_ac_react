package com.android.okalman.kiwi_app.dependencyInjection;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by okalman on 17.10.17.
 */
@Scope
@Retention(RetentionPolicy.CLASS)
public @interface KiwiApplicationScope {
}
