package com.android.okalman.kiwi_app.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by okalman on 16.10.17.
 */

@Dao
public interface FlightsDao {

    @Query("SELECT * FROM Flight")
    List<Flight> getAllFlights();

    @Query("SELECT * FROM Flight WHERE dayToShow == :dayToShow AND invalidated == 0")
    List<Flight> getFlightsForToday(long dayToShow);

    @Query("SELECT * FROM Flight WHERE id == :id")
    Flight getFlightById(String id);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    Long insertFlight(Flight flight);

    @Update
    void updateFlights(List<Flight> flights);



}
