package com.android.okalman.kiwi_app.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Created by okalman on 1.10.17.
 * Object holding flight data and information for database storage
 */
@Entity
@JsonIgnoreProperties(ignoreUnknown=true)
public class Flight implements Serializable {


    @PrimaryKey
    @NonNull
    private String id;
    private String cityFrom;
    private String fromCountry;
    private String fromCountryCode;
    private String cityTo;
    private String toCountry;
    private String toCountryCode;
    private String mapIdfrom;
    private String mapIdto;
    private String stopOvers;
    private String duration;
    private double distance;
    private double price;
    private long departureTime, arrivalTime, dayToShow;
    private boolean invalidated;

    public Flight() {

    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCityFrom() {
        return cityFrom;
    }

    public void setCityFrom(String cityFrom) {
        this.cityFrom = cityFrom;
    }

    public String getFromCountry() {
        return fromCountry;
    }

    @JsonProperty("countryFrom")
    public void setCountrySpecific(HashMap<String,String> countryFrom) {
        this.fromCountry = countryFrom.get("name");
        this.fromCountryCode = countryFrom.get("code");
    }

    public void setFromCountry(String fromCountry) {
        this.fromCountry = fromCountry;
    }

    public String getCityTo() {
        return cityTo;
    }

    public void setCityTo(String cityTo) {
        this.cityTo = cityTo;
    }

    public String getToCountry() {
        return toCountry;
    }

    @JsonProperty("countryTo")
    public void setToCountrySpecific(HashMap<String, String> countryTo) {
        this.toCountry = countryTo.get("name");
        this.toCountryCode = countryTo.get("code");
    }

    public void setToCountry(String toCountry) {
        this.toCountry = toCountry;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(long departureTime) {
        this.departureTime = departureTime;
    }

    @JsonProperty("dTime")
    public void setDepartureTimeInSeconds(long departureTimeSeconds) {
        this.departureTime = departureTimeSeconds*1000;
    }

    public long getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(long arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    @JsonProperty("aTime")
    public void setArrivalTimeInSeconds(long arrivalTimeSeconds) {
        this.arrivalTime = arrivalTimeSeconds*1000;
    }

    public long getDayToShow() {
        return dayToShow;
    }

    public void setDayToShow(long dayToShow) {
        this.dayToShow = dayToShow;
    }

    public boolean isInvalidated() {
        return invalidated;
    }

    public void setInvalidated(boolean invalidated) {
        this.invalidated = invalidated;
    }

    public String getMapIdfrom() {
        return mapIdfrom;
    }

    public void setMapIdfrom(String mapIdfrom) {
        this.mapIdfrom = mapIdfrom;
    }

    public String getMapIdto() {
        return mapIdto;
    }

    public void setMapIdto(String mapIdto) {
        this.mapIdto = mapIdto;
    }

    public String getFromCountryCode() {
        return fromCountryCode;
    }

    public void setFromCountryCode(String fromCountryCode) {
        this.fromCountryCode = fromCountryCode;
    }

    public String getToCountryCode() {
        return toCountryCode;
    }

    public void setToCountryCode(String toCountryCode) {
        this.toCountryCode = toCountryCode;
    }

    public String getStopOvers() {
        return stopOvers;
    }

    @JsonProperty("route")
    public void setStopOvers(List<HashMap<String,Object>> route) {
        String stopOvers="";
        for(int i=1; i<route.size(); i++){
            stopOvers = stopOvers + (String) route.get(i).get("cityFrom");
            if (i < route.size() - 1) {
                 stopOvers = stopOvers + ",";
             }
        }
    }

    public void setStopOvers(String stopOvers) {
        this.stopOvers=stopOvers;
    }

    public String getDuration() {
        return duration;
    }

    @JsonProperty("fly_duration")
    public void setDuration(String duration) {
        this.duration = duration;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getIdForPictureFrom() {
        if (mapIdfrom == null || fromCountryCode == null) {
            return null;
        } else {
            return mapIdfrom + "_" + fromCountryCode.toLowerCase();
        }
    }

    public String getIdForPictureTo() {
        if (mapIdto == null || toCountryCode == null) {
            return null;
        } else {
            return mapIdto + "_" + toCountryCode.toLowerCase();
        }
    }

}
