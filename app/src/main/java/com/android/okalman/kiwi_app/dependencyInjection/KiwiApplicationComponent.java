package com.android.okalman.kiwi_app.dependencyInjection;

import com.android.okalman.kiwi_app.MainActivity;
import com.android.okalman.kiwi_app.fragments.FlightItemFragment;

import dagger.Component;

/**
 * Created by okalman on 17.10.17.
 */

@KiwiApplicationScope
@Component(modules = {PersistenceModuleModule.class,RepositoryModule.class})
public interface KiwiApplicationComponent {

    void injectMainActivityActivity(MainActivity mainActivity);
    void inject(FlightItemFragment flightItemFragment);
}
