package com.android.okalman.kiwi_app.viewModel;

import com.android.okalman.kiwi_app.data.FlightArray;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by okalman on 14.10.17.
 */

public interface SkypickerWebSerivce {

    @GET("/flights")
    Call<FlightArray> getFlights(@QueryMap HashMap<String,String> params);


}
