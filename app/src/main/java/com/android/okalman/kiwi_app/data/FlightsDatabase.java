package com.android.okalman.kiwi_app.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created by okalman on 16.10.17.
 */

@Database(entities = Flight.class,version = 3,exportSchema = false)
public abstract class FlightsDatabase extends RoomDatabase {


    public abstract FlightsDao flightDao();
}
