package com.android.okalman.kiwi_app.viewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.io.File;

/**
 * Created by okalman on 10/18/17.
 */

public class ImageViewModel extends ViewModel {
    private ImageRepository imageRepository;

    ImageViewModel(ImageRepository imageRepository){
        this.imageRepository = imageRepository;
    }

   public LiveData<File> getImage(String imageId, String path){
        return imageRepository.getPicture(imageId,path);
    }

}
