package com.android.okalman.kiwi_app;

import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.android.okalman.kiwi_app.adapters.FlightPagerAdapter;
import com.android.okalman.kiwi_app.data.Flight;
import com.android.okalman.kiwi_app.viewModel.FlightViewModel;
import com.android.okalman.kiwi_app.viewModel.FlightViewModelFactory;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

/**
 * Main an only activity in this project
 */
public class MainActivity extends AppCompatActivity {


    private ContentLoadingProgressBar progressBar;
    private Toolbar toolbar;
    private ViewPager pager;
    private List<Flight> flights;
    private FlightViewModel viewModel;
    private Disposable errorSubscriber;
    @Inject
    FlightViewModelFactory flightViewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((KiwiApplication)getApplication()).getKiwiApplicationComponent()
                .injectMainActivityActivity(this);
        setupViews();
        setupViewModel();

    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        if(errorSubscriber != null && !errorSubscriber.isDisposed()){
            errorSubscriber.dispose();
        }
    }

    private void setupViewModel(){
        viewModel = ViewModelProviders.of(this,flightViewModelFactory)
                .get(FlightViewModel.class);
        viewModel.getFlights().observe(this, flights -> {
            setFlights(flights);
        });
        errorSubscriber = viewModel.getErrorObservable().subscribe(throwable -> handleErrors(throwable));

    }

    private void setupViews() {
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        pager = (ViewPager) findViewById(R.id.pager);
        progressBar = (ContentLoadingProgressBar) findViewById(R.id.progressBar);
        toolbar.setTitle(R.string.title);
        progressBar.show();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
    }

    private void setFlights(List<Flight> flights){
        if(flights !=null){
            pager.setAdapter(new FlightPagerAdapter(getSupportFragmentManager(),flights));
            progressBar.hide();
            Log.d("LOG FLIGHTS: ","emmited");
            for(Flight flight : flights){
                Log.d("LOG FLIGHTS", flight.getId());
            }
        }

    }

    private void handleErrors(Throwable t){
        Log.w("Error", "handling error");
        connectionErrorDialog();
    }

    private void connectionErrorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setPositiveButton("Retry", (dialog, id) -> {
            dialog.dismiss();
            viewModel.forceRefresh();
        });
        builder.setNegativeButton("Close App", (dialog, id) -> {
            this.finish();
        });
        builder.setTitle(R.string.connection_failed_title);
        builder.setMessage(R.string.connection_failed_message)
                .show();
    }
}
