package com.android.okalman.kiwi_app.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.android.okalman.kiwi_app.data.Flight;
import com.android.okalman.kiwi_app.fragments.FlightItemFragment;

import java.util.List;

/**
 * Created by okalman on 3.10.17.
 * This provides data to ViewPager on activity
 */

public class FlightPagerAdapter extends FragmentStatePagerAdapter {

    private List<Flight> flights;

    public FlightPagerAdapter(FragmentManager fm, List<Flight> flights) {
        super(fm);
        this.flights = flights;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new FlightItemFragment();
        Bundle args = new Bundle();
        Flight flight = flights.get(position);
        args.putSerializable("flight", flight);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getCount() {
        return flights.size();
    }
}
