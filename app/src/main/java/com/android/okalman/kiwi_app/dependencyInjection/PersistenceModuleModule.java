package com.android.okalman.kiwi_app.dependencyInjection;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.android.okalman.kiwi_app.data.FlightsDao;
import com.android.okalman.kiwi_app.data.FlightsDatabase;

import dagger.Module;
import dagger.Provides;

/**
 * Created by okalman on 17.10.17.
 */

@Module(includes = KiwiApplicationModule.class)
public class PersistenceModuleModule {

    @Provides
    @KiwiApplicationScope
    public FlightsDao flightsDao(FlightsDatabase db){
        return db.flightDao();
    }

    @Provides
    @KiwiApplicationScope
    public FlightsDatabase flightsDatabase(Context context){
        FlightsDatabase db = Room.databaseBuilder(context,
                FlightsDatabase.class, "flights.db").build();
        return db;

    }

}
