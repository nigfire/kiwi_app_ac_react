package com.android.okalman.kiwi_app.viewModel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import javax.inject.Inject;

/**
 * Created by okalman on 17.10.17.
 */

public class FlightViewModelFactory implements ViewModelProvider.Factory {
    private final FlightRepository repository;

    @Inject
    FlightViewModelFactory(FlightRepository repository){
        this.repository=repository;
    }
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
            if(modelClass.isAssignableFrom(FlightViewModel.class)){
                return (T) new FlightViewModel(repository);
            }else{
                throw new IllegalArgumentException("You can't use this factory with provided class");
            }

    }

}
