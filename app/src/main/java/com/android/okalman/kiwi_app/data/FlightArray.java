package com.android.okalman.kiwi_app.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by okalman on 16.10.17.
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class FlightArray {

    @JsonProperty("data")
    public List<Flight> flights;
}
