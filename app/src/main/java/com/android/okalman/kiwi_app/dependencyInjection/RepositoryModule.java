package com.android.okalman.kiwi_app.dependencyInjection;

import com.android.okalman.kiwi_app.data.FlightsDao;
import com.android.okalman.kiwi_app.viewModel.FlightRepository;
import com.android.okalman.kiwi_app.viewModel.ImageRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by okalman on 17.10.17.
 */
@Module(includes = PersistenceModuleModule.class)
public class RepositoryModule {

    @Provides
    @KiwiApplicationScope
    FlightRepository flightRepository(FlightsDao flightsDao){
        return new FlightRepository(flightsDao );
    }

    @Provides
    @KiwiApplicationScope
    ImageRepository imageRepository(){
        return  new ImageRepository();
    }
}
