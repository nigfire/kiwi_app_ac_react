package com.android.okalman.kiwi_app.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.okalman.kiwi_app.KiwiApplication;
import com.android.okalman.kiwi_app.R;
import com.android.okalman.kiwi_app.data.Flight;
import com.android.okalman.kiwi_app.viewModel.ImageViewModel;
import com.android.okalman.kiwi_app.viewModel.ImageViewModelFactory;

import java.io.File;
import java.text.SimpleDateFormat;

import javax.inject.Inject;

/**
 * Created by okalman on 3.10.17.
 * Fragment which displays flight on activity
 */

public class FlightItemFragment extends Fragment {

    @Inject
    ImageViewModelFactory  imageViewModelFactory;
    ImageViewModel imageViewModel;

    private ImageView destinationPictureView;
    private String toId;
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.pager_item_fragment_layout, container, false);
        Bundle args = getArguments();
        Flight flight = (Flight) args.getSerializable("flight");

        ((TextView) rootView.findViewById(R.id.textTo)).setText(flight.getCityTo() + " (" + flight.getToCountry() + ")");
        ((TextView) rootView.findViewById(R.id.textPrice)).setText(String.valueOf((int) flight.getPrice()) + " Eur");

        String departure = flight.getCityFrom() + " - " + new SimpleDateFormat("dd.MM.yyyy HH:mm").format(flight.getDepartureTime());
        String arrival = flight.getCityTo() + " - " + new SimpleDateFormat("dd.MM.yyyy HH:mm").format(flight.getArrivalTime());
        ((TextView) rootView.findViewById(R.id.textDeparture)).setText(departure);
        ((TextView) rootView.findViewById(R.id.textArrival)).setText(arrival);
        ((TextView) rootView.findViewById(R.id.textDuration)).setText(flight.getDuration());
        ((TextView) rootView.findViewById(R.id.textDistance)).setText(String.valueOf(flight.getDistance())+" Km");
        ((TextView) rootView.findViewById(R.id.textStopovers)).setText(flight.getStopOvers());
        destinationPictureView =  rootView.findViewById(R.id.imageTo);
        toId = flight.getMapIdto() + "_" + flight.getToCountryCode().toLowerCase();
        ((KiwiApplication)getContext().getApplicationContext()).getKiwiApplicationComponent()
                .inject(this);
        imageViewModel = ViewModelProviders.of(this,imageViewModelFactory)
                .get(ImageViewModel.class);
        imageViewModel.getImage(toId, getContext().getFileStreamPath(toId+"jpg").getPath())
                .observe(this, file -> setImage(file));
        return rootView;

    }


    private void setImage(File file){
       destinationPictureView.setImageBitmap(BitmapFactory.decodeFile(file.getPath()));
    }

}
