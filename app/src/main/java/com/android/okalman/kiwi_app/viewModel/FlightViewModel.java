package com.android.okalman.kiwi_app.viewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.android.okalman.kiwi_app.data.Flight;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;


/**
 * Created by okalman on 14.10.17.
 */

public class FlightViewModel extends ViewModel {
    private LiveData<List<Flight>> flights;
    private FlightRepository flightsRepository;
    private PublishSubject<Throwable> errorsSubject;

    public FlightViewModel(FlightRepository flightsRepository) {
        errorsSubject = PublishSubject.create();
        this.flightsRepository=flightsRepository;
        flightsRepository.getErrorObservable().subscribe(throwable -> handleError(throwable));
        if (flights ==null){
            flights=flightsRepository.getFlights();
        }

     }


    public LiveData<List<Flight>> getFlights() {

        return flights;
    }

    public void forceRefresh(){
        flights=flightsRepository.getFlights();
    }

    public Observable<Throwable> getErrorObservable(){
        return errorsSubject;
    }
    private void handleError(Throwable t){
        errorsSubject.onNext(t);
    }

}
