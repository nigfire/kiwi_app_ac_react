package com.android.okalman.kiwi_app.viewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.android.okalman.kiwi_app.Utils;
import com.android.okalman.kiwi_app.data.Flight;
import com.android.okalman.kiwi_app.data.FlightsDao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by okalman on 14.10.17.
 */
public class FlightRepository {

    FlightsDao flightsDao;
    private static final String serverURL="https://api.skypicker.com/";
     MutableLiveData<List<Flight>>data = new MutableLiveData<>();
     private PublishSubject<Throwable> errorsSubject;

    public FlightRepository(FlightsDao flightsDao){
        this.flightsDao = flightsDao;
        errorsSubject = PublishSubject.create();
    }

    public LiveData<List<Flight>> getFlights(){

        Single.fromCallable(() -> {
            return getInternalFlights();
        }).
                subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(flights -> handleSuccess(flights),throwable ->handleError(throwable));

        return data;

    }

    public Observable<Throwable> getErrorObservable(){
        return errorsSubject;
    }

    private List<Flight> getInternalFlights() throws Exception{
        invalidateOld();
        List<Flight> databaseResult = flightsDao.getFlightsForToday(Utils.getCurrentDayAsLong());
        if(databaseResult !=null && databaseResult.size()==5){
            Log.i("LOADING", "LOADED FROM DB");
            return databaseResult;
        }
        invalidateToday();
        Log.i("LOADING", "LOADED FROM SERVER");
        fillDatabase();
        return  flightsDao.getFlightsForToday(Utils.getCurrentDayAsLong());

    }



    private void fillDatabase() throws Exception{
        int counter = 0;
        List<Flight> flights = fetchFromServer();
        Collections.shuffle(flights);
        for(Flight flight : flights){
            flight.setDayToShow(Utils.getCurrentDayAsLong());
            if(flightsDao.insertFlight(flight)>0){
                counter++;
                Log.d("Insert new", "flight: " + flight.getId());
            }{
                Log.d("Insert failed", "flight: " + flight.getId());
            }
            if (counter == 5 ) break;
        }

    }


    private List<Flight> fetchFromServer() throws Exception{
        try {
            OkHttpClient timeoutClient = new OkHttpClient.Builder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS).build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(serverURL)
                    .addConverterFactory(JacksonConverterFactory.create())
                    .client(timeoutClient)
                    .build();
            return retrofit.create(SkypickerWebSerivce.class).getFlights(Utils.getParamsForApi())
                    .execute().body().flights;
        }catch (Exception e){
            throw  new IllegalStateException(e);
        }


    }

    private void invalidateToday(){
        List<Flight> flights = flightsDao.getFlightsForToday(Utils.getCurrentDayAsLong());
        if(flights != null) {
            for (Flight flight : flights) {
                Log.d("Invalidate today", "Flight invalidated: " + flight.getId());
                flight.setInvalidated(true);
            }
            flightsDao.updateFlights(flights);
        }
    }

    private void invalidateOld(){
       List<Flight> flights = flightsDao.getAllFlights();
       if(flights != null){
           for(Flight flight : flights){
               if(flight.getDayToShow()< Utils.getCurrentDayAsLong()){
                   Log.d("Invalidate old", "Flight invalidated: " +flight.getId());
                   flight.setInvalidated(true);
               }
           }
           flightsDao.updateFlights(flights);
       }
    }

    private void handleSuccess(List<Flight> flights){
        Log.d("Handle Success", "success");
        data.setValue(flights);


    }

    private void handleError(Throwable t){
        Log.w("Repository ", t);
        errorsSubject.onNext(t);

    }


}
