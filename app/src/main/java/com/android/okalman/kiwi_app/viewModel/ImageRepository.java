package com.android.okalman.kiwi_app.viewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by okalman on 10/18/17.
 */

public class ImageRepository {
    private static final String remoteUrl="https://images.kiwi.com/photos/600/";

    @Inject
    public ImageRepository(){}

    private HashMap<String, MutableLiveData<File>> cache = new HashMap<>();
    LiveData<File> getPicture(String imageId, String path){
        if(cache.get(path) == null){
            cache.put(imageId, new MutableLiveData<>());
        }
        Single.fromCallable(() ->
                loadOrDownload(imageId,path)
        )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSuccess(downloadedFile -> onDownloaded(downloadedFile,imageId))
                .doOnError(t -> onDownloadError(t))
                        .subscribe();

        return cache.get(imageId);
    }
    private File loadOrDownload(String imageId, String path) throws Exception{
        File file = new File(path);
        if(file.exists()){
            Log.d("IMAGE LOADER", "Image is present on storage: " +imageId);
            return file;
        }else {
            Log.d("IMAGE LOADER", "Image is not present on storage: " +imageId);
            file.createNewFile();
          FileOutputStream fileOutputStream = new FileOutputStream(file);
          fileOutputStream.write(downloadImage(imageId));
          return file;
        }
    }

    private void onDownloaded(File f, String imageId){
        cache.get(imageId).setValue(f);
    }

    private void onDownloadError(Throwable t){
        Log.w("IMAGE DOWNLOADER: ", t);
    }

    private byte[] downloadImage(String id) throws Exception {
        URL obj = new URL(remoteUrl + id + ".jpg");
        Log.d("IMAGE DOWNLOADER", "DOWNLOADING: " + remoteUrl + id + ".jpg");
        InputStream in = new BufferedInputStream(obj.openStream());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        int n = 0;
        while (-1 != (n = in.read(buf))) {
            out.write(buf, 0, n);
        }
        out.close();
        in.close();
        Log.d("IMAGE DOWNLOADER", "Downloaded:" + out.size() + " bytes");
        return out.toByteArray();
    }
}
