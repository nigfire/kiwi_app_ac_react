package com.android.okalman.kiwi_app.viewModel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import javax.inject.Inject;

/**
 * Created by okalman on 10/18/17.
 */

public class ImageViewModelFactory implements ViewModelProvider.Factory {
    ImageRepository imageRepository;

    @Inject
    public ImageViewModelFactory(ImageRepository imageRepository){
        this.imageRepository=imageRepository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if(modelClass.isAssignableFrom(ImageViewModel.class)){
            return (T) new ImageViewModel(imageRepository);
        }else {
            throw new IllegalArgumentException("You can't use this factory with provided class");
        }
    }
}
