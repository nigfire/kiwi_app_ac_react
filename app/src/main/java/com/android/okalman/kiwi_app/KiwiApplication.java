package com.android.okalman.kiwi_app;

import android.app.Application;

import com.android.okalman.kiwi_app.dependencyInjection.DaggerKiwiApplicationComponent;
import com.android.okalman.kiwi_app.dependencyInjection.KiwiApplicationComponent;
import com.android.okalman.kiwi_app.dependencyInjection.KiwiApplicationModule;

/**
 * Created by okalman on 17.10.17.
 */

public class KiwiApplication extends Application {

    private  KiwiApplicationComponent kiwiApplicationComponent;

    @Override
    public void onCreate(){
        super.onCreate();

        kiwiApplicationComponent = DaggerKiwiApplicationComponent.builder()
                .kiwiApplicationModule(new KiwiApplicationModule(this))
                .build();
    }

    public KiwiApplicationComponent getKiwiApplicationComponent(){
        return kiwiApplicationComponent;
    }
}
