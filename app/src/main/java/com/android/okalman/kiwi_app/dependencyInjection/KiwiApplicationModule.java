package com.android.okalman.kiwi_app.dependencyInjection;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by okalman on 17.10.17.
 */
@Module
public class KiwiApplicationModule {

    private final Context context;

    public KiwiApplicationModule(Context context){
        this.context=context;
    }

    @Provides
    @KiwiApplicationScope
    public Context context(){
        return context;
    }
}
